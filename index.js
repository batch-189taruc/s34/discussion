// EXPRESS SETUP
// ~import expres by using the 'require' directive
const express = require('express')
// ~use the express() function and assign it to the app variable
const app = express()
// ~declare a variable for the port of this server
const port = 3000

// ~use these middleware to enable our server to read JSON as regular JS
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))

// ~ you can have your routes after all of that.
// ROUTES

// GET route
app.get('/', (req, res) => {
	res.send('Hello World!')
})

// POST route
app.post('/hello', (req,res) =>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

// REGISTER user route
let users = []

app.post('/register',(req, res) => {
	console.log(req.body)

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)
		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send('Please input BOTHusername and password.')
	}

})

app.put('/change-password',(req, res) => {
	let message = ''

	for(let i = 0; i <= users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password

			message = `User ${req.body.username}'s password has been updated!`

			break;
		} else {
			message = 'User does not exist.'
		}
	}
	res.send(message)
})

app.listen(port, () => console.log(`Server is running at port ${port}`))

